const functions = require("firebase-functions");
const admin = require("firebase-admin");
admin.initializeApp()

const algoliasearch = require('algoliasearch').default;

const ALGOLIA_ID = 'BWYK1RSK2S' //functions.config().algolia.app_id;
const ALGOLIA_ADMIN_KEY = '08381487652d571c518e5e14a4bd6cd5' //functions.config().algolia.api_key;

const ALGOLIA_INDEX_NAME = 'dares';
const client = algoliasearch(ALGOLIA_ID, ALGOLIA_ADMIN_KEY);

exports.removeDares = functions.https.onCall(async (data,context)=>{
    if (!context.auth) return {status: 'error', code: 401, message: 'Not signed in'}
    if(!context.auth.token.admin) return {status: 'error', code: 401, message: 'Not an admin'}
    
    const index = client.initIndex(ALGOLIA_INDEX_NAME);
    const output = await index.deleteObject(data.id);
    return output
  });

exports.makeDares = functions.https.onCall(async (data,context)=>{
  if (!context.auth) return {status: 'error', code: 401, message: 'Not signed in'}
  // if(!context.auth.token.admin) return {status: 'error', code: 401, message: 'Not an admin'}


    data.objectID = data.id

  const index = client.initIndex(ALGOLIA_INDEX_NAME);
  index.saveObject(data);
  return true
}); 

exports.addAdmin = functions.https.onCall(async (data,context)=>{

    
    if (!context.auth) return {status: 'error', code: 401, message: 'Not signed in'}
    if(
        (!context.auth.token.admin && context.auth.token.email != 'sam.cross@gmx.com')
    ) return {status: 'error', code: 401, message: 'Not an admin',test: context.auth.token.email}
    const user = await admin.auth().getUserByEmail(data.email)
    admin.auth().setCustomUserClaims(user.uid,{
        admin:true
    })
    return true
})







// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//   functions.logger.info("Hello logs!", {structuredData: true});
//   response.send("Hello from Firebase!");
// });
