<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


$router->get('/emailTokenFail', ['as' => 'emailTokenFail', function () use ($router) {
    return $router->app->version();
}]);
$router->get('/emailToken', ['as' => 'emailToken', function () use ($router) {
    return $router->app->version();
}]);
$router->post('auth/login', 'AuthController@login');
$router->post('auth/register', 'AuthController@register');
$router->get('auth/ConfirmationEmail', 'AuthController@ConfirmationEmail');

$router->group([

    'middleware' => 'auth',
    'prefix' => 'auth'

], function () use($router) {
    $router->post('logout', 'AuthController@logout');
    $router->post('refresh', 'AuthController@refresh');
    $router->get('me', 'AuthController@me');

});

$router->get('/', function(){
    return view('main');
});

$router->get('/{catchall}', function(){
    return view('main');
});
