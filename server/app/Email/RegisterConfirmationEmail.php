<?php

namespace App\Email;

use Illuminate\Mail\Mailable;
use App\User;

class RegisterConfirmationEmail extends Mailable
{
    public $user;
    /**
     * Create a new message instance.
     *
     * @param  \App\Models\Order  $order
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('RegisterConfirmationEmail', ["user" => $this->user] );
    }
}