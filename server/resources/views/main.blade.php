<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
		<link rel="manifest" href="/site.webmanifest">
		<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
		<meta name="msapplication-TileColor" content="#00aba9">
		<meta name="msapplication-TileImage" content="/mstile-144x144.png">
		<meta name="theme-color" content="#ffffff">
		<link rel="manifest" href="manifest.webmanifest" />
		<script src="sw.js"></script>
		<title>dareAlt</title>
		<link rel="stylesheet" href="style.css">
	</head>
	<body>
		<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

		<script
			defer
			data-ad-client="ca-pub-6093513796480739"
			src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"
		></script>
		<script
			defer
			src="https://cdn.jsdelivr.net/npm/algoliasearch@4.5.1/dist/algoliasearch-lite.umd.js"
		></script>
		<script
			defer
			src="https://cdn.tutorialjinni.com/elasticlunr/0.9.6/elasticlunr.min.js"
		></script>
		<script
			defer
			src="https://cdn.jsdelivr.net/npm/lodash@4.17.21/lodash.min.js"
			integrity="sha256-qXBd/EfAdjOA2FGrGAG+b3YBn2tn5A6bhz+LSgYD96k="
			crossorigin="anonymous"
		></script>
		<script
			defer
			src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js"
			integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0"
			crossorigin="anonymous"
		></script>
		<script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>
		<link
			defer
			href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css"
			rel="stylesheet"
			integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl"
			crossorigin="anonymous"
		/>
		<link
			defer
			rel="stylesheet"
			href="https://cdn.jsdelivr.net/gh/JNKKKK/MoreToggles.css@0.2.1/output/moretoggles.min.css"
		/>
		<link
			defer
			rel="stylesheet"
			href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
			integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN"
			crossorigin="anonymous"
		/>
		<script type="module" src="js/app.js"></script>
		<div id="app"></div>
		<script
			src="https://browser.sentry-cdn.com/6.2.3/bundle.tracing.min.js"
			integrity="sha384-gDTsbUCgFQKbxNZj/RvveTOuAPZgNMjQzMdsD2TI/7YSPN+r49xERr43VxADcGVV"
			crossorigin="anonymous"
		></script>
		<script>
			Sentry.init({
			dsn: "https://5138c8fb314e49b1a2fe06ad3288eb98@o559854.ingest.sentry.io/5695044",
			release: "test",
			integrations: [new Sentry.Integrations.BrowserTracing()],

			// We recommend adjusting this value in production, or using tracesSampler
			// for finer control
			tracesSampleRate: 1.0,
			});
			if ("serviceWorker" in navigator) {
				window.addEventListener("load", function () {
					navigator.serviceWorker.register("/sw.js").then(
						function (registration) {
							// Registration was successful
							console.log(
								"ServiceWorker registration successful with scope: ",
								registration.scope
							);
						},
						function (err) {
							// registration failed 🙁
							console.log("ServiceWorker registration failed: ", err);
						}
					);
				});
			}
		</script>
	</body>
</html>
