import html from './html.js'
import events from './events.js'
// import users from './components/users.js'
// import dares from './components/dares.js'
// import game from './components/game.js'
// import tags from './components/tags.js'
import login from './components/login.js'
// import darescloud from './components/cloudDare.js'
import help from './components/help.js'


var app = new Vue({
  el: '#app',
  data: {
    message: 'Hello Vue!',
    page: 'login',
    user:null,
  },
  mounted() {
    const this_ = this
    events.$on('/auth/me',user => {
      this_.user = user
      this_.setPage('help')
    })
  },
  methods: {
    setPage(page) {
      this.page = page
    },
    logout(){
      this.user = null
      this.setPage('login')
    }
  },
  components: { login, help},
  // components: { users, dares, game,tags,login,darescloud,help},
  template: html`
    <span  >
    <nav class="navbar navbar-dark " style="background-color: #115566;">
        <div class="container-fluid" style='color:#ffffff'>
        <h1 v-if="page == 'game'"><i class="fa fa-trophy" aria-hidden="true"></i> Game</h1>
        <h1 v-if="page == 'tags'"><i class="fa fa-list" aria-hidden="true"></i> Tags</h1>
        <h1 v-if="page == 'users'"><i class="fa fa-user" aria-hidden="true"></i> My Players</h1>
        <h1 v-if="page == 'dares'"><i class="fa fa-download" aria-hidden="true"></i> My Dares</h1>
        <h1 v-if="page == 'darescloud'"><i class="fa fa-cloud" aria-hidden="true"></i> Community Dares</h1>
        <h1 v-if="page == 'help'"><i class="fa fa-info-circle" aria-hidden="true"></i> Help</h1>
        <h1 v-if="page == 'login'">login</h1>
          <h1 v-if="user"><a style='color:#ffffff' ref='#' @click='logout()'> logout</a></h1>
        </div>
      </nav>
      <div v-if="user" class="position-absolute bottom-0 start-0 end-0" style=' z-index:10'>
    
        <nav class="navbar navbar-dark " style="background-color: #115566;">
          <div class="container-fluid justify-content-around">
            <button v-if="user?.isAdmin" type="button" class="btn nofocus"
              :class="{'btn-outline-light':page == 'tags','btn-light':page != 'tags' }"
              @click="setPage('tags')"><i class="fa fa-tags" aria-hidden="true"></i>
</button>
<button type="button" class="btn nofocus" :class="{'btn-outline-light':page == 'help','btn-light':page != 'help' }"
              @click="setPage('help')"><i class="fa fa-info-circle" aria-hidden="true"></i>
</button>
            <button type="button" class="btn nofocus" :class="{'btn-outline-light':page == 'darescloud','btn-light':page != 'darescloud' }"
              @click="setPage('darescloud')"><i class="fa fa-cloud" aria-hidden="true"></i>
</button>
            <button type="button" class="btn nofocus" :class="{'btn-outline-light':page == 'dares','btn-light':page != 'dares' }"
              @click="setPage('dares')"><i class="fa fa-list" aria-hidden="true"></i>
</button>

            <button type="button" class="btn nofocus" :class="{'btn-outline-light':page == 'users','btn-light':page != 'users' }"
              @click="setPage('users')"><i class="fa fa-user" aria-hidden="true"></i>
</button>
            <button type="button" class="btn nofocus" :class="{'btn-outline-light':page == 'game','btn-light':page != 'game' }"
              @click="setPage('game')"><i class="fa fa-trophy" aria-hidden="true"></i>
</button>
          
            </div>
        </nav>
      </div>
       <div  class="container " style='overflow:scroll; height:90%; padding-bottom:110px'>
<!-- 
        <game v-if="page == 'game'"></game>
        <tags v-if="page == 'tags'"></tags>
        <users v-if="page == 'users'"></users>
        <dares v-if="page == 'dares'"></dares>
        <darescloud v-if="page == 'darescloud'"></darescloud>-->
        <help v-if="page == 'help'"></help>
        <login v-if="page == 'login'"></login> 
      </div>

      
    
    </span>
    `
  })