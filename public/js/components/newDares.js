import html from '../html.js'
import api from '../api/dares.js'
import tagPicker from './tagPicker.js'
import guid from '../guid.js'
import userdata from './userdata.js'
import tags from '../api/tags.js'
import tagcloud from './newDaresParts/tagcloud.js'
const tagsref = firebase.database().ref('tags')
export default {
    props: ['guid'],
    data() {
        return {
            maxpage: 6,
            data: {
                tags: [],
                outcomeTags: [],
                isOtherPlayer: false,
                OtherPlayerOutcomeTags: [],
                OtherPlayerTags: [],
                name: '',
                runones: false,
                isNSFW: false,
                description: '{{player}}',
            },
            page: 0,
            tags: [],
            error: null,
        }
    },
    components: { tagPicker, tagcloud },
    mounted() {
        this.load()
    },

    watch: {
        guid() {
            this.loadDare()
        }
    },
    methods: {
        getTag(id){
            return _.find(this.tags,['id',id])
        },
        prePage() {
            this.page--
            if (this.page < 0) { this.page = 0 }
        },
        nextPage() {
            if (!this.data.isOtherPlayer && this.page == 3) {
                this.page = 0
            }else{
                this.page++
            }
            if (this.page == this.maxpage) { this.page = 0 }
        },
        load() {
            tagsref.on('value', snap => {
                this.tags = snap.val()
            })
        },
        loadDare() {
            userdata('dares/' + this.guid).on('value', snap => {
                this.data = snap.val()
            })
        },
        makeNew() {
            this.data = {
                name: '',
                tags: [],
                isOtherPlayer: false,
                OtherPlayerOutcomeTags: [],
                OtherPlayerTags: [],
                outcomeTags: [],
                runones: false,
                isNSFW: false,
                description:'{{player}}',
            }
        },
        removeisOtherPlayer() {
            if (!this.data.isOtherPlayer && this.page == 2) {
                this.page = 1
            }
            if (!this.data.isOtherPlayer) {
                this.data.OtherPlayerOutcomeTags = []
                this.data.OtherPlayerTags = []
            }
        },
        setpage(page) {
            if (!this.data.isOtherPlayer) return
            this.page = page
        },
        save({ clone = false } = {}) {
            if (!this.data.name) return this.error = 'no name found'
            const id = this.guid || guid()
            userdata('dares/' + id).set(
                { id, ...this.data }
            );
            if (!clone) {
                this.makeNew()
            }
            this.$parent.search()
            this.close()
        },
        addtag(tag) {
            this.data.description = this.data.description + tag + '  '

        },
        close() {
            this.$parent.guid = null
            setTimeout(() => {
                this.page = 0
            }, 100)
        },
        isHidonTag(id) {
            const tag = _.find(this.tags, ['id', id])
            return tag.name.substring(0, 1) == '_';
        },
        runones() {
            const runonesid = _.find(this.tags, tag => tag?.name == '_run_ones_').id
            if (_.find(this.data.tags, tag => tag == runonesid)) {
                const newtags = _.filter(this.data.tags, tag => tag != runonesid)
                this.data.tags = newtags
                const newoutcomeTags = _.filter(this.data.outcomeTags, tag => tag != runonesid)
                this.data.outcomeTags = newoutcomeTags
                this.data.runones = false
                return
            }
            this.data.runones = true

            this.data.tags = this.data.tags || []
            this.data.outcomeTags = this.data.outcomeTags || []
            this.data.tags.push(runonesid)
            this.data.outcomeTags.push(runonesid)
        }
    },
    template: html`
    <div>
        <button type="button" class="btn btn-primary" data-bs-toggle="modal" @click="makeNew()" data-bs-target="#newDare">
            add
        </button>
        <div class="modal fade" id="newDare" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
            aria-labelledby="newDareLabel" aria-hidden="true">
            <div class="modal-dialog ">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="newDareLabel">{{this.data.name || 'New Dare'}}</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"
                            @click="close()"></button>
                    </div>
                   

                    <div class="card-body" v-if="page == 0">
                        <h4>Name</h4>
                        <input class="form-control" v-model="data.name">
                        <h4>Dare text <small>(what shows up in game)</small></h4>
                        <textarea class="form-control p-3" v-model="data.description"></textarea>
                        <p></p>
                        <button type="button" class="btn btn-secondary" @click="addtag('{{player}}')">add
                            player</button>
                        <button type="button" class="btn btn-secondary" v-if="data.isOtherPlayer"
                            @click="addtag('{{other}}')">add other
                            player</button>
                        <p></p>
                        <div class='d-flex p-3 justify-content-between'>
                            <div>
                                <h4>NSFW</h4>
                                <p>If the dare contains adult content</p>
                            </div>
                                <div class="mt-ios-red" style="font-size:9px">
                                    <input id="isNSFW" type="checkbox" name="isNSFW" v-model="data.isNSFW" />
                                    <label for="isNSFW"></label>
                                </div>
                            
                        </div>
                        <div class='d-flex p-3 justify-content-between'>
                            <div>
                                <h4>Two Player</h4>
                                <p>alows the dare to have a seconed player</p>
                            </div>
                            <div class="mt-ios " style="font-size:9px">
                                <input id="OtherPlayerTags" type="checkbox" name="OtherPlayerTags"
                                    v-model="data.isOtherPlayer" />
                                <label for="OtherPlayerTags"></label>
                            </div>
                        </div>
                        <div class='d-flex p-3 justify-content-between'>
                            <div>
                                <h4>Run Ones</h4>
                                <p>Make the dare runs per user</p>
                            </div>
                            <div class="mt-ios" style="font-size:9px" @click='runones()'>
                                <input :id="runones" type="checkbox" :name="runones" :checked="data.runones" />
                                <label for="runones"></label>
                            </div>
                        </div>
                        <hr >
                        <h2 >Player Tags</h2>
                        <hr>
                        <div class='d-flex p-3 justify-content-between'>
                            <div >
                                <h4>Player Tags needs</h4>
                                <p>the player will only get this dare if thay have all the folowing tags</p>
                                <span
                                v-for="(tag) in data?.tags"
                                class="badge badge-pill m-1 bg-primary text-white"
                                >
                                    {{getTag(tag).name}}
                                </span>
                            </div>
                            <button type="button" class="btn btn-primary align-self-end" @click="page = 1">
                                SetUp
                            </button>
                        </div>
                        <div class='d-flex p-3 justify-content-between'>
                            <div >
                                <h4>blocking tags for Player</h4>
                                <p>the player will only get this dare if none of the folowing tags are selected</p>
                                <span
                                v-for="(tag) in data?.tagsNotNeeded"
                                class="badge badge-pill m-2 bg-primary text-white"
                                >
                                    {{getTag(tag).name}}
                                </span>
                            </div>
                            <button type="button" class="btn btn-primary align-self-end" @click="page = 2">
                                SetUp
                            </button>
                        </div>
                        <div class='d-flex p-3 justify-content-between'>
                            <div >
                                <h4>Outcome Tags</h4>
                                <p>if this dare is completed the folowing tags will be removed from the player</p>
                                <span
                                v-for="(tag) in data?.outcomeTags"
                                class="badge badge-pill m-2 bg-primary text-white"
                                >
                                    {{getTag(tag).name}}
                                </span>
                            </div>
                            <button type="button" class="btn btn-primary align-self-end" @click="page = 3">
                                SetUp
                            </button>
                        </div>
                        <hr v-if='data.isOtherPlayer' >
                            <h2  v-if='data.isOtherPlayer' >Other Player Tags</h2>
                        <hr v-if='data.isOtherPlayer' >
                        <div class='d-flex p-3 justify-content-between' v-if='data.isOtherPlayer'>
                            <div >
                                <h4>Other Player</h4>
                                <p>this dare will find another player with all of the folowing tags</p>
                                <span
                                v-for="(tag) in data?.OtherPlayerTags"
                                class="badge badge-pill m-2 bg-primary text-white"
                                >
                                    {{getTag(tag).name}}
                                </span>
                            </div>
                            <button type="button" class="btn btn-primary align-self-end" @click="page = 4">
                                SetUp
                            </button>
                        </div>
                        <div class='d-flex p-3 justify-content-between' v-if='data.isOtherPlayer'>
                            <div >
                                <h4>blocking tags for other Player Tags</h4>
                                <p>this dare will find another player that as not got the folowing tags</p>
                                <span
                                v-for="(tag) in data?.tagsNotNeededOther"
                                class="badge badge-pill m-2 bg-primary text-white"
                                >
                                    {{getTag(tag).name}}
                                </span>
                            </div>
                            <button type="button" class="btn btn-primary align-self-end" @click="page = 5">
                                SetUp
                            </button>
                        </div>
                        <div class='d-flex p-3 justify-content-between' v-if='data.isOtherPlayer'>
                            <div >
                                <h4>Other Player Outcome</h4>
                                <p>if this dare is completed the folowing tags will be removed from the Other player</p>
                                <span
                                v-for="(tag) in data?.OtherPlayerOutcomeTags"
                                class="badge badge-pill m-2 bg-primary text-white"
                                >
                                    {{getTag(tag).name}}
                                </span>
                            </div>
                            <button type="button" class="btn btn-primary align-self-end" @click="page = 6">
                                SetUp
                            </button>
                        </div>
                    </div>
                    
                    <div class="card-body" v-if="page == 1">
                        <h1>Player Tags needed</h1>
                        <p>the player will only get this dare if thay have all the folowing tags</p>
                        <tagPicker :showNSFW='data.isNSFW' v-model="data.tags" title=''></tagPicker>
                        
                    </div>
                    <div class="card-body" v-if="page == 2">
                        <h1>blocking tags for Player</h1>
                        <p>the player will only get this dare if none of the folowing tags are selected</p>
                        <tagPicker :showNSFW='data.isNSFW' v-model="data.tagsNotNeeded" title=''></tagPicker>
                    </div>
                    <div class="card-body" v-if="page == 3">
                        <h1>Outcome Tags</h1>
                        <p>if this dare is completed the folowing tags will be removed from the player </p>
                        <tagPicker :showNSFW='data.isNSFW' v-model="data.outcomeTags" title=''></tagPicker>
                    </div>
                    <div class="card-body" v-if="page == 4">
                        <h1>Other Player</h1>
                        <p>this dare will find another player with all of the folowing tags</p>
                        <tagPicker :showNSFW='data.isNSFW' v-model="data.OtherPlayerTags" title=''></tagPicker>
                    </div>
                    <div class="card-body" v-if="page == 5">
                        <h1>blocking tags for other Player Tags</h1>
                        <p>this dare will find another player that as not got the folowing tags</p>
                        <tagPicker :showNSFW='data.isNSFW' v-model="data.tagsNotNeededOther" title=''></tagPicker>
                    </div>
                    <div class="card-body" v-if="page == 6">
                        <h1>Other Player Outcome</h1>
                        <p>if this dare is completed the folowing tags will be removed from the Other player </p>
                        <tagPicker :showNSFW='data.isNSFW' v-model="data.OtherPlayerOutcomeTags" title=''></tagPicker>
                    </div>
                    <div class="card-body" v-if="page == 7">
                        <h1></h1>
                        <p>enter th text below for what you like to show up for the dare</p>
                        
                    </div>

                    <div class="modal-footer" v-if="page == 0">
                        <button type="button" class="btn btn-danger" data-bs-dismiss="modal" @click="close()">Close</button>
                        <button type="button" class="btn btn-primary float-end" data-bs-dismiss="modal" @click="save()">
                            save
                        </button>
                        <button type="button" class="btn btn-secondary float-end" @click="save({clone:true})">
                            save and clone
                        </button>
                    </div>
                    <div class="modal-footer" v-if="page != 0">
                        <button type="button" class="btn btn-primary" @click="page = 0">back</button>
                    </div>
                </div>
            </div>
        </div>
        </div>
    `
}