import html from '../html.js'
import api from '../api.js'
export default {
    data() {
        return {
            email: '',
            password: '',
            error: null,
            isEmailLogin: true,
            window
        }
    },
    mounted() {

    },
    methods: {
        login: async function () {
            try{
            const user = await api.login(this.email,this.password)
        }catch(e){
            this.error = e.message
        }
        },
        register: async function () {
        },
    },
    template: html`
    <div>
        <div class="mb-3"></div>
        <div v-if="error" class="alert alert-danger" role="alert">
            {{error}}!
        </div>

        <div class="mb-3">
            <label for="emailInput" class="form-label">Email</label>
            <input v-model="email" id='emailInput' type="email" class="form-control" placeholder="email">
        </div>
        <div class="mb-3">
            <label for="passInput" class="form-label">Password</label>
            <input v-model="password" id='passInput' type="password" class="form-control" placeholder="password">
        </div>
        <div class="btn-group" role="group" aria-label="Basic example">


            <button type="button" class="btn btn-primary float-end" @click="login()">
                login
            </button>

            <button type="button" class="btn btn-secondary float-end" @click="register()">
                register
            </button>
        </div>
    </div>
    `
}