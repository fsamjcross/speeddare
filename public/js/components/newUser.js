import html from '../html.js'
import tagPicker from './tagPicker.js'
import userdata from './userdata.js'
import guid from '../guid.js'

export default {
    props:['guid'],
    data() {
        return {
            data:{
                name:'',
                tags:[]
            },
            nsfw:false,
            error:null,
        }
    },
    components:{tagPicker},
    mounted() {
        this.load()
    },
    watch:{
        guid(){
            this.loadUser()
        }
    },
    methods: {
        load(){

        },
        loadUser(){
            if(!this.guid) return
             userdata('users/'+this.guid).on('value',snap=>{
                this.data  = snap.val()
            })
        },
        makeNew(){
            this.data = {
                name:'',
                tags:[]
            }
        },
        save(){
            if(!this.data.name) return this.error = 'no name found'
            this.close()
            const id =this.guid || guid()
            userdata('users/'+ id).set(
                {id, ...this.data}
            );
            this.makeNew()
            
        },
        close(){
            this.$parent.guid = null
        }
    },
    template: html`
    <div>
        <button type="button" class="btn btn-primary" @click="makeNew()" data-bs-toggle="modal" data-bs-target="#newUser">
            add
        </button>
        <div class="modal fade" id="newUser" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
            aria-labelledby="newUserLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="newUserLabel">{{data.name || 'New Player'}}</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div v-if="error">{{error}}</div>
                        <div class="mb-3">
                            <label for="UserNameInput" class="form-label">player name</label>
                            <input id='UserNameInput' type="text" class="form-control" v-model="data.name" placeholder="name">
                        </div>
                        <div class="mb-3 d-flex justify-content-between " >
                            <div>show adult content</div>
                            <div class="mt-ios-red" style="font-size:9px"> 
                                <input id="NSFW" type="checkbox" name="NSFW" v-model="nsfw"/>
                                <label for="NSFW"></label>
                            </div>
                         </div>
                         <tagPicker class='mt-3' :showNSFW="nsfw" v-model="data.tags"></tagPicker>
                        </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal" @click="close()">Close</button>
                        <button type="button" class="btn btn-primary float-end" data-bs-dismiss="modal" @click="save()">
                            save
                        </button>
                    </div>
                </div>
            </div>
        </div>
        </div>
    `,
    templateold: html`
    <div>
        <button type="button" class="btn btn-primary" @click="makeNew()" data-bs-toggle="modal" data-bs-target="#newUser">
            add
        </button>
        <div class="modal fade" id="newUser" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
            aria-labelledby="newUserLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="newUserLabel">new User</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div v-if="error">{{error}}</div>
                        <div class="form-check">
                            <input class="form-check-input" v-model="nsfw" type="checkbox" value="" id="nsfwCheck">
                            <label class="form-check-label" for="nsfwCheck">
                                show nsfw
                            </label>
                            </div>
                        <div class="mb-3">
                            <label for="UserNameInput" class="form-label">users</label>
                            <input id='UserNameInput' type="text" class="form-control" v-model="data.name" placeholder="User">
                        </div>
                        <tagPicker :showNSFW="nsfw" v-model="data.tags" title='Starting Tags'></tagPicker>
                        </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal" @click="close()">Close</button>
                        <button type="button" class="btn btn-primary float-end" data-bs-dismiss="modal" @click="save()">
                            save
                        </button>
                    </div>
                </div>
            </div>
        </div>
        </div>
    `
}