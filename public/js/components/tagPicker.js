import html from '../html.js'
import tagPickerSection from './tagPickerSection.js'
const tagsref = firebase.database().ref('tags')
export default {
    props:['value','showNSFW'],
    components:{tagPickerSection},
    data() {
        return {
            data:{}
        }
    },
    mounted() {
        this.data = this.value;
    },
    watch:{
        value(){
            this.data = this.value;
        },
        data() {
            this.$emit('input', this.data)
        },
    },
    template: html`
    <div>
        <tagPickerSection class='mt-3' :showNSFW="showNSFW" v-model="data" title='Sex' :allow='["Male","Female"]'></tagPickerSection>
        <tagPickerSection class='mt-3' :showNSFW="showNSFW" v-model="data" title='Clothing' :allow='["Socks","Shoes","Bottoms","Top","bra","Underwear"]'></tagPickerSection>
        <tagPickerSection class='mt-3' :showNSFW="showNSFW" v-model="data" title='Social' :allow='["Facebook","Youtube","Instagram","Snapchat","Reddit"]'></tagPickerSection>
        <tagPickerSection class='mt-3' :showNSFW="showNSFW" v-model="data" title='Other' :block='["Male","Female","Facebook","Youtube","Instagram","Snapchat","Reddit","Socks","Shoes","Bottoms","Top","bra","Underwear"]'></tagPickerSection>
    </div>
    `
}