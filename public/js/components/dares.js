import html from '../html.js'
import api from '../api/dares.js'
import newDares from './newDares.js'
import userdata from './userdata.js'
import checkalart from './checkalart.js'
const tagsref = firebase.database().ref('tags')
export default {
    data() {
        return {
            auth: firebase.auth(),
            data: null,
            error: null,
            guid: null,
            isNSFW: false,
            showTags: false,
            user: null,
            searchString: '',
            resolts: [],
            index: elasticlunr(function () {
                this.addField('name');
                this.addField('description');
                this.addField('tags');
                this.setRef('id');
            })
        }
    },
    components: { newDares, checkalart },
    mounted() {
        userdata('dares').on('value', snap => {
            this.data = snap.val()
            this.addSeach()
        })
        tagsref.on('value', snap => {
            this.tags = Object.values(snap.val() || {})
            this.addSeach()
        })
        this.auth.onAuthStateChanged(async User => this.user = User)
    },
    methods: {
        search() {
            this.resolts = Object.values(this.index.search(this.searchString).map(el => el.ref));
        },
        addSeach() {
            this.index = elasticlunr(function () {
                this.addField('name');
                this.addField('description');
                this.addField('tags');
                this.setRef('id');
            })

            for (const key in this.data) {
                const dare = this.data[key]
                const tags = this.getallDares(dare).map(el => el?.name).join(' ')
                this.index.addDoc({
                    id: dare.id,
                    name: dare.name,
                    description: dare.description,
                    tags: tags
                });
            }
        },
        find(id) {
            return _.find(this.resolts, el => el == id)
        },
        getTag(id) {
            return _.find(this.tags, ['id', id])
        },
        setguid(id) {
            this.guid = id
            search()
        },
        remove(id) {
            userdata('dares/' + id).remove()
            search()
        },
        checknsfw(dares) {
            if (!dares.isNSFW) return true
            if (this.isNSFW == true) return true
            return false
        },
        getallDares(dare) {
            return [
                ...dare.tags || [],
                ...dare.tagsNotNeeded || [],
                ...dare.outcomeTags || [],
                ...dare.OtherPlayerOutcomeTags || [],
                ...dare.OtherPlayerTags || [],
                ...dare.tagsNotNeededOther || [],
            ].map(this.getTag)
        },
        async publish(id) {
            const dare = _.find(this.data, ['id', id])
            firebase.database().ref('dares/' + id).set(
                { id, ...dare, userid: this.user.uid, accepted: "false" }
            );
            userdata('dares/' + id).set(
                { id, ...dare, publish: "true" }
            );
            const allTags = this.getallDares(dare)
            const functions = firebase.functions()
            const output = await firebase.functions().httpsCallable('makeDares')(
                { id, allTags, NSFW: dare.isNSFW, name: dare.name, description: dare.description, userid: this.user.uid, accepted: "false" }
            )

        }
    },
    template: html`
    <div>
        <div class="alert alert-info m-3" role="alert" v-if="!data">
            You need to add dares to play, you can make them with the add button or get them from the public list
        </div>
    
        <div class="card m-3">
            <div class="card-body">
                <div class="input-group mb-3">
                    <input type="text" class="form-control" v-model="searchString" placeholder="searchString"
                        aria-label="searchString" @input="search()">
    
                    <div class="input-group-prepend">
                        <button @click='search()' class="btn btn-secondary" type="button">Search</button>
                    </div>
                </div>
                <div class="form-check">
                    <input class="form-check-input" v-model="isNSFW" type="checkbox" value="" id="nsfwCheck">
                    <label class="form-check-label" for="nsfwCheck">
                        show nsfw
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" v-model="showTags" type="checkbox" value="" id="showTags">
                    <label class="form-check-label" for="showTags">
                        show tags
                    </label>
    
                </div>
            </div>
        </div>
    
        <table class="table" style='width: 100%;'>
            <tbody>
                <tr v-for="(dare, index) in data" :key="index"
                    v-if="(isNSFW || !dare.isNSFW) && (find(dare.id) || resolts.length == 0 )">
                    <th scope="row">
                        <p>{{dare.name}} </p>
                        <p class="p-1" style='font-size:75%'>{{dare.description}}</p>
                    </th>
                    <td v-if="showTags">
                        <span v-if="dare.isNSFW" class="badge badge-pill m-1 bg-danger text-white">NSFW</span>
                        <span v-for="(tag) in getallDares(dare)" class="badge badge-pill m-1 bg-primary text-white">
                            {{tag.name}}
                        </span>
                    </td>
                    <td>
                        <checkalart :modalid="'pubalart'+ dare.id"
                            message='Are you sure you would like to publish this dare to the public list?'
                            :action="()=> publish(dare.id)" />
                        <button v-if="!dare?.publish" type="button" class="btn btn-success" data-bs-toggle="modal"
                            :data-bs-target="'#pubalart'+ dare.id">
                            <i class="fa fa-rocket" aria-hidden="true"></i>
                        </button>
                        <button type="button" @click="setguid(dare?.id)" class="btn btn-primary" data-bs-toggle="modal"
                            data-bs-target="#newDare">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                        </button>
                        <checkalart :modalid="'remove'+ dare.id" :action="()=> remove(dare.id)" />
                        <button type="button" class="btn btn-danger" data-bs-toggle="modal"
                            :data-bs-target="'#remove'+ dare.id">
                            <i class="fa fa-trash" aria-hidden="true"></i>
                        </button>
    
                    </td>
                </tr>
            </tbody>
        </table>
        <newDares :guid='guid' style='margin-top:50px'></newDares>
    </div>
    `
}