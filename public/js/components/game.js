import html from '../html.js'
import dares from '../api/dares.js'
import users from '../api/users.js'
import tags from '../api/tags.js'
import userdata from './userdata.js'

const tagsref = firebase.database().ref('tags')
export default {
    data() {
        return {
            daresAll: {},
            dares: {},
            activeDare: null,
            users: {},
            activeUserNumber: 0,
            otherPlayer: null,
            error: null,
            tags: {},
            started: false,
            nsfw: false,
        }
    },
    mounted() {
        this.load()
    },
    watch: {
        nsfw(){
          this.loadDares()      
        },
        daresAll() {
            this.loadDares()
        }
    },
    methods: {
        loadDares(){
                this.dares = _.filter(this.daresAll, dare => {
                    if(typeof dare.isNSFW == 'undefined' ) return false
                    if(this.nsfw) return true
                    if(dare.isNSFW) return false
                    return true
                })
        },
        getallDares(dare) {
            return [
                ...dare.tags || [],
                ...dare.tagsNotNeeded || [],
                ...dare.outcomeTags || [],
                ...dare.OtherPlayerOutcomeTags || [],
                ...dare.OtherPlayerTags || [],
                ...dare.tagsNotNeededOther || [],
            ].map(this.getTag)
        },
        getTag(id) {
            return _.find(this.tags, ['id', id])
        },
        getDare(id) {
            return _.find(this.dares, ['id', id])
        },
        next() {
            this.activeUserNumber++
            if (!this.activeUser || !this.activeUser.name) {  //if no user
                this.activeUserNumber = 0
            }
            this.loadnext()
        },
        loadnext(){
            this.activeDare = this.getRandamDare()
            if (!this.activeDare) {
                this.load()
                return this.started = false
            }
            this.setupOtherPlayer()
        },
        setupOtherPlayer() {
            if (!this.activeDare.isOtherPlayer) return this.otherPlayer == null

            const posableOthers = _.filter(this.users, user => {
                if (this.activeUser.id == user.id) return false

                if (this.activeDare.tagsNotNeededOthers) for (const tag of this.activeDare.tagsNotNeededOther) {
                    if (_.find(user.tags, tag)) return false;
                }
                if (this.activeDare.OtherPlayerTags){
                    for (const tag of this.activeDare.OtherPlayerTags) {
                        if (!user.tags) return false
                        if (_.find(user.tags, tag) ) return false;
                    }
                }else{
                    return false
                }
                    
                return true
            })
            this.otherPlayer = _.sample(posableOthers)
            if(this.activeUser?.id == this.otherPlayer?.id ) throw "otherplayer is active player"
            if(!this.otherPlayer) this.loadnext();
        },
        success() {
            this.removeOutcomeTagsFromUser()
            this.removeOutcomeTagsFromOtherPlayer()
            this.users.map(user => {
                this.setupPossibleDares(user)
            })
            this.next()
        },
        fail() {
            this.next()
        },
        start() {
            this.users.map(user => {
                this.setupPossibleDares(user)
            })
            this.started = true

            this.next()
        },
        removeOutcomeTagsFromUser() {
            return this.removeOneArrayFromOther(this.activeUser.tags, this.activeDare.outcomeTags)
        },
        removeOutcomeTagsFromOtherPlayer() {
            if (!this.otherPlayer) return
            return this.removeOneArrayFromOther(this.otherPlayer.tags, this.activeDare.OtherPlayerOutcomeTags)
        },
        removeOneArrayFromOther(arrayA, arrayB) {
            _.remove(
                arrayA,
                (n) =>
                    _.findIndex(arrayB, (o) => o == n) != -1

            );
        },
        load() {
            userdata('dares').on('value', snap => {
                this.daresAll = Object.values(snap.val() || {})
            })
            userdata('users').on('value', snap => {
                this.users = Object.values(snap.val() || {})
                this.users.map(user => {

                })
            })
            tagsref.on('value', snap => {
                this.tags = Object.values(snap.val() || {})
            })

        },
        setupPossibleDares(user) {

            //add all dares the user can use
            user.possibleDares = this.dares.filter(dare => {
                // return false if any tagsNotNeeded are in tags
                if (dare.tagsNotNeeded) for (const tag of dare.tagsNotNeeded) {
                    if (_.find(user.tags, tag)) {

                        return false;
                    }
                }
                if (!dare.tags) {
                    return true
                }
                if (dare.tags?.length == 0) {
                    return true
                }
                return _.intersection(user.tags, dare.tags).length > 0;
            })
            delete user.possibleDares.push
            delete user.possibleDares.splice
            return user
        },
        getRandamDare() {
            return _.sample(this.activeUser.possibleDares)
        },
    },
    computed: {
        activeUser() {
            return Object.values(this.users)[this.activeUserNumber]
        },
        title() {
            const description = this.activeDare.description || this.activeDare.name
            return description
                .split('{{player}}').join(this.activeUser?.name)
                .split('{{other}}').join(this.otherPlayer?.name)
        }

    },
    template: html`
    <div class="d-flex d-flex justify-content-center align-items-center flex-column " style='height:100%;'>
        <h1 v-if="daresAll.length < 2">you need at least 2 dares to play.</h1>
        <p v-if="daresAll.length < 2">you can get dare from the pulic list </p>
        <h1 v-if="users.length < 2">you need at least 2 players to play.</h1>
        <div v-if="users.length >= 2">
            <div v-if="!started" class="form-check">
                <input class="form-check-input" v-model="nsfw" type="checkbox" value="" id="nsfwCheck">
                <label class="form-check-label" for="nsfwCheck">
                    show nsfw
                </label>
            </div>
            <button type="button" v-if="!started" class="btn btn-success btn-lg" @click='start()'>start</button>
            <div v-if="started">
                <h1 v-if="activeDare">{{title}}</h1>
                <div v-if='!true' v-for="(user, index) in users" :key="index" style="width:100px;">
                    <h4>{{user.name}}</h4>
                    <span v-for="tag in user?.tags">{{getTag(tag)?.name}}, </span>
                </div>
                <div class='mb-3 mt-3'>
                    <span v-if="activeDare.isNSFW" class="badge badge-pill m-1 bg-danger text-white">NSFW</span>
                    <span v-for="(tag) in getallDares(activeDare)" class="badge badge-pill m-1 bg-primary text-white">
                        {{tag.name}}
                    </span>
                </div>
                <button type="button" class="btn btn-danger btn-lg" @click='fail()'>Fail</button>
                <button type="button" class="btn btn-success btn-lg" @click='success()'>Success</button>
                <button type="button" class="btn btn-warning btn-lg" @click='loadnext()'>Skip</button>
            </div>
        </div>
    </div>
    </div>
    `
}
