import html from '../html.js'

import userdata from './userdata.js'
const tagsref = firebase.database().ref('tags')
export default {
    props:['value','title','showNSFW','allow','block'],
    data() {
        return {
            error: null,
            loading:true,
            tags:{},
        }
    },
    mounted() {
        this.load()
            this.loading = true
            this.data = this.value;
            this.loading =false
    },
    methods: {
        load(){
            tagsref.on('value',snap=>{
                this.tags = snap.val()
            })
        },
        ischecked(id){
            //if(this.data.length == 0 ) return false
            const tag = _.find(this.data,key => key == id)
            return tag
        },
        toggle(id){
            if(this.ischecked(id)) return this.leaveTag(id)
            return this.addTag(id)
        },
        leaveTag(id){
           this.data =_.filter(this.data,key => key != id)
           this.$emit('input', this.data)
        },
        addTag(id){
            this.leaveTag(id)
           this.data.push(id)
           this.$emit('input', this.data)
        },
        checknsfw(tag){
            if(!tag.nsfw ) return true
            if(this.showNSFW == true) return true
            return false
        },
        isHidonTag(tag){
            if(tag.name.substring(0,1) == '_'){
                return true;
            }
            if(this.block){
                return _.includes(this.block,tag.name)
            }
            if(this.allow){
                return !_.includes(this.allow,tag.name)
            }
            return false
        }
    },
    watch:{
        value(){
            this.loading = true
            this.data = this.value;
            this.loading =false 
        },
        data() {
            this.$emit('input', this.data)
        }
    },    
    template: html`
    <div v-if="!loading">
        <h4>{{title}}</h4>
            <div v-for="tag in tags" v-if='checknsfw(tag) && !isHidonTag(tag)' class='d-flex p-1 justify-content-between'>
                <div>{{tag.name}}</div>
                <div>
                    <div class="mt-ios" style="font-size:9px"> 
                        <input :id="tag.id+title" type="checkbox" :name="tag.id+title" @click='toggle(tag.id)'  :checked="ischecked(tag.id)"/>
                        <label :for="tag.id+title"></label>
                    </div>
                </div>
        </div>
    </div>
    `
}