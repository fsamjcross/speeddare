import html from '../html.js'
const daresref = firebase.database().ref('/dares')
const tagsref = firebase.database().ref('tags')
import checkalart from './checkalart.js'
import userdata from './userdata.js'

export default {
    data() {
        return {
            auth: firebase.auth(),
            data: [],
            tags: [],
            showTags: false,
            isNSFW: false,
            mydares: {},
            user: null,
            searchString: '',
            index: null,
            hits: [],
            page: 0,
            maxPage: 0,
        }
    },
    mounted() {
        const client = algoliasearch('BWYK1RSK2S', '10798d578086c9303c381490bf8d9393');
        this.index = client.initIndex('dares');
        userdata('dares').on('value', snap => {
            this.mydares = snap.val()
            this.mydares = this.mydares || []
        })
        this.auth.onAuthStateChanged(async User => {
            if (User) {
                const token = await User.getIdTokenResult()
                this.user = User
                this.user.claims = token.claims
            }
        })
        this.search()
    },
    watch: {
        page() {
            if (this.page < 0) this.page = 0
            if (this.page > this.maxPage) this.page = this.maxPage
            this.search()
        }
    },
    components: { checkalart },
    methods: {
        async search() {
            const out = await this.index.search(this.searchString, { page: this.page })
            this.page = out.page
            this.hits = out.hits
            this.maxPage = out.nbPages - 1
        },
        getTag(id) {
            return _.find(this.tags, ['id', id])
        },
        async remove(id) {

            const output = await firebase.functions().httpsCallable('removeDares')(
                { id }
            )
            console.log(output);
            await firebase.database().ref('dares/' + id).remove()
            this.hits = null

            this.search()
        },
        async add(id) {
            const dare = await firebase.database().ref('dares/' + id).get()
            userdata('dares/' + id).set(
                { ...dare.val(), publish: "true" }
            );
        },
        check(id) {
            return !this.mydares[id]
        }
    },
    template: html`
    <div>
        <div class="alert alert-info m-3" role="alert" v-if="!mydares">
            You can add dares from this public list to save you making your own.
        </div>
        <div class="card m-3">
            <div class="card-body">
                <div class="input-group mb-3">
                    <input type="text" class="form-control" v-model="searchString" placeholder="searchString"
                        aria-label="searchString" @change="search()">
    
                    <div class="input-group-prepend">
                        <button @click='search()' class="btn btn-secondary" type="button">Search</button>
                    </div>
                </div>
                <div class="form-check">
                    <input class="form-check-input" v-model="isNSFW" type="checkbox" value="" id="nsfwCheck">
                    <label class="form-check-label" for="nsfwCheck">
                        show nsfw
                    </label>
                </div>
            </div>
        </div>
    
    
    
        <table class="table" style='width: 100%;'>
            <tbody>
                <tr v-for="(dare, index) in hits" :key="index" v-if='isNSFW || !dare.NSFW'>
                    <th scope="row">
                        {{dare.name}}
                        <p class="p-1" style='font-size:75%'>{{dare.description}}</p>
                    </th>
                    <td>
    
                        <span v-if="dare.NSFW" class="badge badge-pill m-1 bg-danger text-white">NSFW</span>
                        <span v-for="(tag) in dare?.allTags" class="badge badge-pill m-1 bg-primary text-white">
                            {{tag.name}}
    
                        </span>
                    </td>
                    <td>
                        <button type="button" class="btn btn-success" v-if="check(dare.id)" @click='add(dare.id)'>
                            <i class="fa fa-plus" aria-hidden="true"></i>
                        </button>
                        <checkalart :modalid="'removealart'+dare.id" :action="()=> remove(dare.id)" />
                        <button v-if="user?.claims.admin == true" type="button" class="btn btn-danger"
                            data-bs-toggle="modal" :data-bs-target="'#removealart'+dare.id">
                            <i class="fa fa-trash" aria-hidden="true"></i>
                        </button>
                    </td>
                </tr>
            </tbody>
        </table>
        <nav aria-label="Page navigation example">
            <ul class="pagination">
                <li class="page-item"><a class="page-link" href="#" @click='page--'>Previous</a></li>
                <li class="page-item" :class="{active:page == i-1}" v-for="i in maxPage+1" :key="i"><a class="page-link"
                        href="#" @click='page = i-1'>{{i}}</a></li>
                <li class="page-item"><a class="page-link" href="#" @click='page++'>Next</a></li>
            </ul>
        </nav>
    </div>
    `
}