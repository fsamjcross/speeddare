import html from '../html.js'

import newTags from './newTags.js'
import userdata from './userdata.js'
import checkalart from './checkalart.js'

export default {
    data() {
        return {
            data: {},
            error: null,
            guid:null,
            nsfw:false
        }
    },
    components: { newTags,checkalart},
    mounted() {
            firebase.database().ref('tags').on('value',snap=>{
                this.data  = snap.val()
            })
    },
    methods: {
        remove(id) {
            firebase.database().ref('tags/'+id).remove()
        },
        checknsfw(tag){
            if(!tag.nsfw ) return true
            if(this.nsfw == true) return true
            return false
        }
    },
    template: html`
    <div > 
            <div class="form-check">
                <input class="form-check-input"  v-model="nsfw"  type="checkbox" value="" id="nsfwCheck">
                <label class="form-check-label"for="nsfwCheck">
                    show nsfw 
                </label>
            </div>
            <ul>
                <li v-for="(tag,index) in data" v-if="checknsfw(tag)" :key="index" style='padding:5px' class='d-flex justify-content-around'>
                    <h5 class=''style='width: 150px;'>{{tag?.name}}</h5>
                    <h5  class=''style='width: 150px;'>
                      <span  v-if="tag?.nsfw" class="">NSFW</span>
                    </h5>
                    <button type="button" @click="() => guid = tag.id" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#newtag">
                        <i class="fa fa-pencil" aria-hidden="true"></i>
                    </button>
                    <checkalart :modalid="'removealart'+tag.id" :action="()=> remove(tag.id)"/>
                        <button type="button" class="btn btn-danger" data-bs-toggle="modal" :data-bs-target="'#removealart'+tag.id">
                            <i class="fa fa-trash" aria-hidden="true"></i>
                    </button>
                </li>
            </ul>
            <newTags :guid='guid'  style='margin-top:50px'></newTags>
    </div>
    `
}