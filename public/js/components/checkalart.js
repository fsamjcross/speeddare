import html from '../html.js'

export default {
    props:['action','message','modalid'],
    template: html`
        <!-- Button trigger modal -->


        <!-- Modal -->
        <div class="modal fade" :id="modalid" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{message ||'Are you sure?'}}</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <!-- <div class="modal-body" v-if="desciptiontext">
                {{desciptiontext}}
            </div> -->
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">no</button>
                <button type="button" class="btn btn-danger"  data-bs-dismiss="modal" @click="action()">yes</button>
            </div>
            </div>
        </div>
        </div>
    `
}