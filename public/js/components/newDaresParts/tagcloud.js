import html from "../../html.js";

export default {
    props: ['data','addtag','isHidonTag'],
    computed: {
        allTags() {
            return [
                ...this.data.tags || [],
                ...this.data.outcomeTags || [],
                ...this.data.OtherPlayerOutcomeTags || []
            ]
        }
    },
    template: html`
    <div class="mb-3">
        <span @click="addtag($parent.$parent.getTag(tag).name)" v-for="(tag) in allTags"
            class="badge badge-pill m-1 bg-danger text-white" v-if="!isHidonTag(tag)">
            {{$parent.$parent.getTag(tag).name}}
        </span>
        <span @click="addtag('{{player}}s ' + $parent.$parent.getTag(tag).name)" v-for="(tag) in data?.tags"
            class="badge badge-pill m-1 bg-primary text-white" v-if="!isHidonTag(tag)">
            {player}'s {{$parent.$parent.getTag(tag).name}}
        </span>
        <span @click="addtag('{{player}}s ' + $parent.$parent.getTag(tag).name)" v-for="(tag) in data?.outcomeTags"
            class="badge badge-pill m-1 bg-success text-white" v-if="!isHidonTag(tag)">
            {player}'s user_outcome_{{$parent.$parent.getTag(tag).name}}
        </span>
        <span @click="addtag('{{other}}s '+ $parent.$parent.getTag(tag).name)" v-for="(tag) in data?.OtherPlayerTags"
            class="badge badge-pill m-1 bg-secondary text-white" v-if="!isHidonTag(tag)">
            {other}'s {{$parent.$parent.getTag(tag).name}}
        </span>
        <span @click="addtag('{{other}}s '+ $parent.$parent.getTag(tag).name)" v-for="(tag) in data?.OtherPlayerOutcomeTags"
            class="badge badge-pill m-1 bg-info text-white" v-if="!isHidonTag(tag)">
            {other}'s {{$parent.$parent.getTag(tag).name}}
        </span>
        <div class="mb-3">
            <button type="button" class="btn btn-secondary" @click="addtag('{{player}}')">add
                player</button>
            <button type="button" class="btn btn-secondary" v-if="data.isOtherPlayer" @click="addtag('{{other}}')">add other
                player</button>
        </div>
    </div>
                        `


}