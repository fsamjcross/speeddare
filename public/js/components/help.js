import html from '../html.js'
export default {
    methods: {
        async load(){
  
        }
    },
    template:html`
        <div class="card " style='z-index: 2;'>
            <div class="card-body">
                <div class="alert alert-info m-3" role="alert">
                    <h1>TLDR</h1>
                    <p>You may like to get started with the default dares</p>
                    <button type="button" class="btn btn-primary btn-lg m-3" @click='load'>get started</button>
                </div>
                
                <h3>Tag based dares</h3>
                <div class='m-3'>
                <p>Dares are chosen using a tag based system. </p>
                <p>All dares have 3 sections per active player.</p>
                <ul>
                    <li>needs - all tags selected are needed for the dare to show for a player</li>
                    <li>blocking - the player must not have any of the blocking tags for the dare to show</li>
                    <li>outcome - tags to be remove from the player ones there dare is completed eg (used for when an item of clothing is removed) </li>
                </ul>
                </div>
                <h3>Pages</h3>
                <div class='m-3'>
                <h4>Community dares</h4>
                <p>This tab has all the community dares on it</p>
                <p>at the top of the page is the search and NSFW filter button to hide adult content</p>
                <p>use the add button to add all the dares you would like in your game</p>
                <h4>My dares</h4>
                <p>this is a list of all your dares that will come up in your games</p>
                <p>prssing the gree spaceship will add the dare to the community list</p>
                <h4>My Players</h4>
                <p>this tag is for you to setup all the players for your games </p>
                </div>
            </div>
        </div>
    `
}