import html from '../html.js'
import tagPicker from './tagPicker.js'
import guid from '../guid.js'

export default {
    props:['guid'],
    data() {
        return {
            data:{
                name:''
            },
            error:null,
        }
    },
    mounted() {
        this.load()
    },
    watch:{
        guid(){
            this.loadtag()
        }
    },
    methods: {
        load(){

        },
        loadtag(){
            if(!this.guid) return
            const id =this.guid || guid()
             firebase.database().ref('tags/'+id).on('value',snap=>{
                this.data  = snap.val()
            })
        },
        makeNew(){
            this.data = {
                name:'',
            }
        },
        save(){
            if(!this.data.name) return this.error = 'no name found'
            const id =this.guid || guid()
            firebase.database().ref('tags/'+id).set(
                {id, ...this.data}
            );
            this.makeNew()
            this.close()
        },
        close(){
            this.$parent.guid = null
        }
    },
    template: html`
    <div>
        <button type="button" class="btn btn-primary" @click="makeNew()" data-bs-toggle="modal" data-bs-target="#newtag">
            add
        </button>
        <div class="modal fade" id="newtag" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
            aria-labelledby="newtagLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="newtagLabel">new tag</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div v-if="error">{{error}}</div>
                        <div class="mb-3">
                            <label for="tagNameInput" class="form-label">tags</label>
                            <input id='tagNameInput' type="text" class="form-control" v-model="data.name" placeholder="tag">
                        </div>
                         <div class="mb-3">
                            <input class="form-check-input" v-model="data.nsfw" type="checkbox" value="" id="nsfwCheckadd">
                            <label class="form-check-label" for="nsfwChecknsfwCheckadd">
                                mark as nsfw 
                            </label>
                        </div>
                        </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal" @click="close()">Close</button>
                        <button type="button" class="btn btn-primary float-end" data-bs-dismiss="modal" @click="save()">
                            save
                        </button>
                    </div>
                </div>
            </div>
        </div>
        </div>
    `
}