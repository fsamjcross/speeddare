import html from '../html.js'
import userdata from './userdata.js'
import newUsers from './newUser.js'
import checkalart from './checkalart.js'

const tagsref = firebase.database().ref('tags')

export default {
    data() {
        return {
            data: {},
            error: null,
            guid:null,
            tags:{},
        }
    },
    components: { newUsers,checkalart },
    mounted() {
        userdata('users').on('value', snap => {
            
            this.data = snap.val()
        })
        tagsref.on('value',snap=>{
                this.tags =   Object.values(snap.val() || {})
            })
    },
    
    methods: {
        remove(id) {
            userdata('users/'+id).remove()
        },
        getTag(id){
            return _.find(this.tags,['id',id])
        },
    },
    template: html`
    <div>
                <table class="table" style='width: 100%;'>

            <tbody>
                <tr v-for="(user, index) in data" :key="index">
                    <th scope="row">{{user?.name}}</th>
                    <td>
                        <span
                            v-if="user.tags"
                            v-for="(tag, index) in user?.tags" :key="index"
                            class="badge badge-pill m-1 bg-primary text-white"
                        >
                            {{getTag(tag)?.name}}
                        </span>
                    </td>
                    <td>
                        <button type="button" @click="() => guid = user.id" class="btn btn-primary" data-bs-toggle="modal"
                            data-bs-target="#newUser">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                        </button>
                    </td>
                    <td>
                        <checkalart :modalid="'removealart'" :action="()=> remove(user.id)"/>
                        <button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#removealart">
                            <i class="fa fa-trash" aria-hidden="true"></i>
                        </button>
                    </td>
                </tr>
            </tbody>
            </table>
            <newUsers :guid='guid' class='mb-3' style='margin-top:50px'></newUsers>
            <button  type="button" v-if="data && Object.keys(data).length >= 2 " @click="$parent.page = 'game'" class="btn btn-success">go to Game</button>

    </div>

`
}