import events  from "./events.js"
class api {
  constructor() {
    this.makeAxios()
    this.config = {
      timeout: 1000,
      headers: {
        'Content-Type': 'application/json;charset=UTF-8',
      }
    }
  }
  async login(email,password) {
    const auth = await this.axios.post('/auth/login',{email,password})
    this.setJWT(auth.data.access_token)
    const user = await this.me()
    events.$emit('/auth/me',user)
    return user
  }
  async me(){
    const user = await this.axios.get('/auth/me')
    events.$emit('/auth/me',user.data)
    return user.data
  }
  setJWT(accessToken){
    this.config.headers.Authorization = `bearer ${accessToken}`
    this.makeAxios()
  }
  makeAxios() {
    this.axios = axios.create(this.config);
  }
}

export default new api()