var CACHE_NAME = 'dare-v2';
var urlsToCache = [
   '/download' 
]

self.addEventListener('install', function (event) {
    // Perform install steps
    self.skipWaiting()
});


// self.addEventListener('fetch', function (event) {
//     event.respondWith(
//         caches.open(CACHE_NAME).then(function (cache) {
//             return cache.match(event.request).then(function (response) {
//                 return response || fetch(event.request).then(function (response) {
//                     cache.put(event.request, response.clone());
//                     return response;
//                 }).catch(function () {
//                     return cache.match('/?page=offline');
//                 });
//             });
//         })
//     );
// });

// self.addEventListener('activate', function (event) {
//     // Calling claim() to force a "controllerchange" event on navigator.serviceWorker
//     event.waitUntil(self.clients.claim());
//     //Delete all cache
//     caches.keys().then(function (cacheNames) {
//         return Promise.all(
//             cacheNames.map(function (cacheName) {
//                 return caches.delete(cacheName);
//             })
//         );
//     }).then(function () {
//         caches.open(CACHE_NAME)
//             .then(function (cache) {
//                 console.log('Added Cache')
//                 return cache.addAll(urlsToCache);
//             })
//         console.log('Opened cache');
//     })

//     console.log('Activated!')
// });